LEMP-docker is a LEMP stack ([Nginx][nginx], [MariaDB][mariadb] and [PHP][php]).

Component | `latest-1804`
---|---
[Nginx][nginx] |`2.4`
[MariaDB][mariadb] |`10.1`
[PHP][php] |`8.0`
[phpMyAdmin][phpmyadmin] |`5.1.1`

* Based on https://github.com/fbraz3/lemp-docker

[nginx]: https://www.nginx.com/
[mariadb]: https://mariadb.org/
[php]: http://php.net/
[phpmyadmin]: https://www.phpmyadmin.net/
[end-of-life]: http://php.net/supported-versions.php
[info-license]: LICENSE

## Using the image
### Open ports
* Web is accessed on http://localhost:8080
* PHPmyadmin is accessed on http://localhost:8080/phpmyadmin
  * Login is 'admin'
  * Password is 'pass'

### Launching in Linux
```bash
# /var/www i mapped to /home/user/github
# Launch the image with autostart when docker start
docker run --restart unless-stopped -d -v /home/user/github:/var/www -v mysql-data:/var/lib/mysql \
-p 8080:80 --name lemp registry.gitlab.com/karye/docker-lemp
```

### Launching in Windows
```powershell
# /var/www i mapped to C:\github
# Launch the image with autostart when docker start
docker run --restart unless-stopped -d -v "C:\github:/var/www" -v "mysql-data:/var/lib/mysql" `
-p 8080:80 --name lemp registry.gitlab.com/karye/docker-lemp
```
